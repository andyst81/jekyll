---
layout: post
title:  "First Real Post"
date:   2019-10-19 01:15:00 +0700
author: Andy T
categories: post update
---

So this is my first mini update of this blog. I've been learning the technologies
I mentioned in my last post and feel like I'm getting the hang of a few things now.
The way this method generates static sites in really incredible. I really like it.
Compared to generating posts in WordPress this really feels like I'm doing something
cool and actually programming, as opposed to just making webpages.

I'll keep learning as I have a long way to go. One thing that I want to learn is
how to deploy an existing code script into a web app. Hopefully I'll get to do that
soon.
