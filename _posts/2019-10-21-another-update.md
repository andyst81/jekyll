---
layout: post
title:  "First Real Post"
date:   2019-10-20 10:15:00 +0700
author: Andy T
categories: post update
---

So I'm getting back into designing websites for fun. I started learning HTML,
CSS and JavaScript several years ago on freecodeCamp and I really enjoyed it,
but somehow got out of practice.
I then found WordPress and built quite a few sites in that, but there wasn't
really much coding involved it that. Over the past few years I've been
practicing using Python quite a bit to write scripts to make my life easier,
some of which I may post here at some point in the future.
Recently I've really been getting into static site generators and think I've
found what I was looking for all those years ago. I love the idea of SSGs and
have been playing around with quite a lot of them, including Gatsby, Jekyll,
Hugo, 11ty (which I'm using for this site now).
I've also been playing around with a few headless CMS too, so that I can create
content on the go, from anywhere. I think I've found the one I like now - Sanity.
I'll post about it some time in the future, along with some of the other tools
I've been playing around with, such as Netlify, Zeit Now, and the other SSGs
that I posted about earlier.
This has been a goal of mine for quite some time and I'm really happy to be
working towards it now. Stay cool everyone - until next time.
