---
layout: page
title: About Me
permalink: /about/
---

![Me](http://gravatar.com/avatar/a8eb0aa9506de0ef867d31b7727c614f){: .center-image }
  
My name is Andy and I am just learning this stuff now. 
It's cool though. I'm really enjoying learning how to do these things.
I'm learning GitLab, ZEIT and Jekyll at the same time to create this site. 
It's very different to what I've been using for web design up to now, 
which is WordPress. 
  
How cool is this?
